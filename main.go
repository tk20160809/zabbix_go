package main

import (
	"zabbix_go/cmd"
)

func main() {
	cmd.Execute()
}
