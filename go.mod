module zabbix_go

go 1.13

require (
	github.com/prometheus/common v0.9.1
	github.com/spf13/cobra v1.3.0
	gopkg.in/yaml.v2 v2.4.0
)
