package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
)

type reqTemplate struct {
	*reqZabbix
	Method string    `json:"method"`
	Params reqParams `json:"params"`
}

type reqTemplateDelete struct {
	*reqZabbix
	Method string   `json:"method"`
	Params []string `json:"params"`
}

type resTemplate struct {
	Result []struct {
		Templateid string `json:"templateid"`
		Name       string `json:"name"`
	} `json:"result"`
}

var groups string

func init() {
	rootCmd.AddCommand(template)
	template.AddCommand(template_get, template_create, template_delete, template_update)
	template_create.Flags().StringVarP(&name, "name", "n", "", "Template name")
	template_create.MarkFlagRequired("name")

	//template_create.Flags().StringVarP(&groupid, "groupid", "g", "", "groupid")
	//template_create.MarkFlagRequired("groupid")
	template_create.Flags().StringVarP(&groups, "groups", "g", "", "group names,...")
	template_create.MarkFlagRequired("groups")

	template_delete.Flags().StringVarP(&name, "name", "n", "", "Template name")
	template_delete.MarkFlagRequired("name")

	// template.update
	template_update.Flags().StringVarP(&name, "name", "n", "", "Template Name")
	template_update.MarkFlagRequired("name")
	//template_update.Flags().StringVarP(&group, "group", "g", "", "Host Group")
	template_update.Flags().StringVarP(&tags, "tags", "", "", "Tag:Value, ... ")
}

var template = &cobra.Command{
	Use:   "template",
	Short: "[get,delete,update, create]",
}

var template_get = &cobra.Command{
	Use:   "get",
	Short: TEMPLATE_GET_METHOD,
	Run: func(cmd *cobra.Command, args []string) {

		params := reqParams{
			Output: "extend",
		}
		req := reqTemplate{rz, TEMPLATE_GET_METHOD, params}

		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		var data resTemplate
		err = json.Unmarshal(res, &data)
		if err != nil {
			log.Fatal(err)
		}
		for _, v := range data.Result {
			fmt.Printf("%s %s\n", v.Templateid, v.Name)
		}
	},
}

var template_create = &cobra.Command{
	Use:   "create",
	Short: TEMPLATE_CREATE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {

		params := reqParams{
			Host: name,
		}
		for _, v := range strings.Split(groups, ",") {
			groupid := getGroupID(v)
			params.Groups = append(params.Groups, Groupid{ID: groupid})
		}
		req := reqTemplate{rz, TEMPLATE_CREATE_METHOD, params}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Println(string(res))
	},
}

var template_delete = &cobra.Command{
	Use:   "delete",
	Short: TEMPLATE_DELETE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {

		templateid := getTemplateID(name)
		req := reqTemplateDelete{rz, TEMPLATE_DELETE_METHOD, []string{templateid}}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Println(string(res))
	},
}

var template_update = &cobra.Command{
	Use:   "update",
	Short: "template.update",
	Run: func(cmd *cobra.Command, args []string) {
		templateid := getTemplateID(name)
		/*
			params := reqJSON{
				Jsonrpc: JSONRPC,
				Method:  TEMPLATE_UPDATE_METHOD,
				ID:      ID,
				Auth:    Config.APIKEY,
				Params: reqParams{
					Templateid: templateid,
				},
			}
		*/
		params := reqParams{
			Templateid: templateid,
			Tags:       reqParamsTags(tags),
		}

		req := reqJSON{rz, TEMPLATE_UPDATE_METHOD, params}

		//params.Params.Tags =reqParamsTags(tags)

		//encoded_params, err := json.Marshal(params)
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}

		res, _ := sendRequest(encoded_params)
		fmt.Printf("%+v\n", string(res))
	},
}

func getTemplateID(name string) string {
	params := reqParams{
		Filter: &reqFilter{
			Name: []string{name},
		},
	}
	req := reqTemplate{rz, TEMPLATE_GET_METHOD, params}
	encoded_params, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := sendRequest(encoded_params)
	var data *resTemplate
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		log.Fatal(err)
	}
	if len(data.Result) == 0 {
		log.Fatal(name + " template does not exist")
	}

	return data.Result[0].Templateid

}
