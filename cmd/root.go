package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

const (
	JSONRPC                = "2.0"
	ID                     = 1
	ExitCodeOK         int = 0
	ExitCodeError      int = 1 + iota
	USER_LOGIN_METHOD      = "user.login"
	HOST_GET_METHOD        = "host.get"
	HOST_CREATE_METHOD     = "host.create"
	HOST_DELETE_METHOD     = "host.delete"
	HOST_UPDATE_METHOD     = "host.update"

	TEMPLATE_GET_METHOD    = "template.get"
	TEMPLATE_CREATE_METHOD = "template.create"
	TEMPLATE_DELETE_METHOD = "template.delete"
	TEMPLATE_UPDATE_METHOD = "template.update"

	GRAPH_GET_METHOD          = "graph.get"
	SCREEN_CREATE_METHOD      = "screen.create"
	SCREEN_GET_METHOD         = "screen.get"
	SCREEN_DELETE_METHOD      = "screen.delete"
	SCREEN_ITEM_CREATE_METHOD = "screenitem.create"
	HOSTGROUP_GET_METHOD      = "hostgroup.get"
	HOSTGROUP_UPDATE_METHOD   = "hostgroup.update"

	MAINTENANCE_GET_METHOD    = "maintenance.get"
	MAINTENANCE_CREATE_METHOD = "maintenance.create"
	MAINTENANCE_DELETE_METHOD = "maintenance.delete"
	MAINTENANCE_UPDATE_METHOD = "maintenance.update"

	ITEM_GET_METHOD    = "item.get"
	ITEM_CREATE_METHOD = "item.create"
	ITEM_DELETE_METHOD = "item.delete"
	ITEM_UPDATE_METHOD = "item.update"
	// host.get  https://www.zabbix.com/documentation/4.0/en/manual/api/reference/host/object

	// アイテム タイプ
	ZABBIX_AGENT   int = 0
	ZABBIX_TRAPPER int = 2

	// アイテム　データ型
	ITEM_NUMERIC_FLOAT int = 0 // 浮動小数
	ITEM_CHAR          int = 1 // 文字列
	ITEM_LOG           int = 2 // ログ
	ITEM_NUMERIC       int = 3 // 整数
	ITEM_TEXT          int = 4 // テキスト
)

// host.get status
var available = []string{`Unknown`, `Available`, `Unavailable`}

var (
	rootCmd = &cobra.Command{Use: "zabbix_go"}
	Config  AuthConfig
	name    string
	group   string
	start   string
	end     string
	id      string
	mname   string
	groupid string
)

type reqJSON struct {
	*reqZabbix
	Method string    `json:"method"`
	Params reqParams `json:"params"`
}

type reqZabbix struct {
	Auth    string `json:"auth"`
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
}
type reqParams struct {
	Output           string   `json:"output,omitempty"`
	SelectInterfaces []string `json:"selectInterfaces,omitempty"`
	//SelectGroups          []string `json:"selectGroups,omitempty"`
	SelectGroups          string   `json:"selectGroups,omitempty"`
	SelectParentTemplates []string `json:"selectParentTemplates,omitempty"`

	// host.update
	Filter    *reqFilter   `json:"filter,omitempty"`
	Hostid    string       `json:"hostid,omitempty"`
	Groups    []Groupid    `json:"groups,omitempty"`
	Templates []Templateid `json:"templates,omitempty"`
	Tags      []reqTag     `json:"tags,omitempty"`

	// maintennace
	SelectTimePeriods string `json:"selectTimeperiods,omitempty"`
	SelectHosts       string `json:"selectHosts,omitempty"`
	SortOrder         string `json:"sortorder,omitempty"`
	SortField         string `json:"sortfield,omitempty"`

	// item.get
	Hostids     []string `json:"hostids,omitempty"`
	Templateids []string `json:"templateids,omitempty"`
	//Templateid  string   `json:"templateids,omitempty"`

	// item.update
	Itemid string `json:"itemid,omitempty"`

	// item.create
	// https://www.zabbix.com/documentation/current/en/manual/api/reference/item/object#host
	Name         string `json:"name,omitempty"`          // (required) 名前
	Type         int    `json:"type,omitempty"`          // (required) タイプ
	Key          string `json:"key_,omitempty"`          // (reqreuid) キー
	ValueType    int    `json:"value_type,omitempty"`    // (required) データ型
	AllowTraps   int    `json:"allow_traps,omitempty"`   // 1 - Allow to accept incoming data.
	TrapperHosts string `json:"trapper_hosts,omitempty"` // Allowed hosts
	Templateid   string `json:"templateid,omitempty"`    // テンプレートID
	Delay        string `json:"delay,omitempty"`
	Trends       string `json:"trends,omitempty"`
	History      string `json:"history,omitempty"`

	// template.create
	//Groupid []string`json:"groupid,omitempty"`
	//Groups reqGroups `json:"groups,omitempty"`
	Host string `json:"host,omitempty"`
}

// host.update
type reqTag struct {
	Tag   string `json:"tag"`
	Value string `json:"value"`
}

// host.update
type Groupid struct {
	ID string `json:"groupid"`
}
type Templateid struct {
	ID string `json:"templateid"`
}
type reqFilter struct {
	Name          []string `json:"name,omitempty"`
	Host          []string `json:"host,omitempty"`
	MaintenanceID []string `json:"maintenanceid,omitempty"`
	Hostid        string   `json:"hostid,omitempty"`
}

var rz *reqZabbix

func Execute() {
	rootCmd.Execute()
}

func init() {

	// disable help sort
	cobra.EnableCommandSorting = false

	// ~/.config/zabbix.yml
	loadConfig()

	getAPIKEY()

	rz = &reqZabbix{Config.APIKEY, JSONRPC, ID}

	// hostgorup.get
	// https://www.zabbix.com/documentation/current/en/manual/api/reference/hostgroup/get
	rootCmd.AddCommand(hostgroup)
	hostgroup.AddCommand(hostgroup_get, hostgroup_update)
	hostgroup_update.Flags().StringVarP(&id, "id", "i", "", "Group ID")
	hostgroup_update.MarkFlagRequired("id")
	hostgroup_update.Flags().StringVarP(&name, "name", "n", "", "New groupo name ")
	hostgroup_update.MarkFlagRequired("name")

}

// loadConfig  ~/.config/zabbixapi.yml から認証情報読み取り
func loadConfig() {
	home := os.Getenv("HOME")
	configFile := filepath.Join(home, ".config", "zabbixapi.yml")
	buf, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal([]byte(buf), &Config)
	if err != nil {
		log.Fatal(err)
	}
}

//func sendRequest(params string) string {

//func sendRequest(params string) []byte {
func sendRequest(params []byte) ([]byte, error) {

	url := Config.URL

	// リクエスト
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(params))
	if err != nil {
		return []byte{}, fmt.Errorf("sendRequest() http.NewRequest() %v\n", err)
	}
	req.Header.Add("Content-type", "application/json-rpc")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return []byte{}, fmt.Errorf("sendRequest() client.Do() %v\n", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return []byte{}, fmt.Errorf("sendRequest() ioutil.ReadAll() %v\n", err)
	}

	if res.Status != "200 OK" {
		return []byte{}, fmt.Errorf("sendRequest() res.Status %v\n", res.Status)
	}

	//return string(body)
	return body, nil
}

func reqParamsTags(tags string) []reqTag {
	var reqtag []reqTag
	if tags != "" {
		for _, v := range strings.Split(tags, ",") {
			tag := strings.Split(v, ":")
			reqtag = append(reqtag, reqTag{
				Tag:   tag[0],
				Value: tag[1],
			})
		}
	}
	return reqtag
}
