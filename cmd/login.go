package cmd

import (
	"encoding/json"
	"log"
)

type reqLogin struct {
	Jsonrpc string         `json:"jsonrpc"`
	Method  string         `json:"method"`
	ID      int            `json:"id"`
	Params  reqLoginParams `json:"params"`
}
type reqLoginParams struct {
	User     string `json:"user"`
	Password string `json:"password"`
}

// user.login のレスポンス受信構造体
type resLogin struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  string `json:"result"`
	ID      int    `json:"id"`
}

// AuthConfig  ログイン情報
type AuthConfig struct {
	APIKEY   string `yaml:"apikey"`
	URL      string `yaml:"url"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

/*
 *  Login ログインして apikey 取得
 *  値の定義と関数呼び出しのみなのでテスト不要
 *  LoadConfig()
 *  sendRequest()
 *  loginUnmarshal()
 *  を個別でテストする
 */
func getAPIKEY() {
	params := &reqLogin{
		Jsonrpc: JSONRPC,
		Method:  USER_LOGIN_METHOD,
		ID:      ID,
		Params: reqLoginParams{
			User:     Config.User,
			Password: Config.Password,
		},
	}
	encoded_params, err := json.Marshal(params)
	if err != nil {
		log.Fatal(err)
	}
	//res := sendRequest(string(encoded_params))
	res, _ := sendRequest(encoded_params)
	loginUnmarshal(res)
}

// login.auth レスとポンス JSON 解析
func loginUnmarshal(res []byte) {
	var resLogin resLogin
	err := json.Unmarshal(res, &resLogin)
	if err != nil {
		log.Fatal(err)
	}
	if resLogin.Result == "" {
		log.Fatal("no apikey")
	}
	Config.APIKEY = resLogin.Result
}
