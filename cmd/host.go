package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
)

type reqHostCreateParams struct {

	// host.create
	Host       string             `json:"host,omitempty"`
	Groups     []reqHostGroup     `json:"groups,omitempty"`
	Interfaces []reqHostInterface `json:"interfaces"`
}
type reqHostGroup struct {
	Groupid string `json:"groupid"`
}
type reqHostInterface struct {
	Type  string `json:"type"`
	Main  string `json:"main"`
	UseIP string `json:"useip"`
	IP    string `json:"ip"`
	DNS   string `json:"dns"`
	Port  string `json:"port"`
}

type reqHostParams struct {

	// host.create
	//Host string `json:"host,omitempty"`
	//Groups reqHostGroups `json:"groups,omitempty"`

	// host.get
	Output                string   `json:"output,omitempty"`
	SelectInterfaces      []string `json:"selectInterfaces,omitempty"`
	SelectGroups          string   `json:"selectGroups,omitempty"`
	SelectParentTemplates []string `json:"selectParentTemplates,omitempty"`

	// host.update
	Filter    reqHostFilter `json:"filter,omitempty"`
	Hostid    string        `json:"hostid,omitempty"`
	Groups    []Groupid     `json:"groups,omitempty"`
	Templates []Templateid  `json:"templates,omitempty"`
}

type reqHostFilter struct {
	Host []string `json:"host,omitempty"`
}

type reqHostCreate struct {
	*reqZabbix
	Method string              `json:"method"`
	Params reqHostCreateParams `json:"params"`
}

type reqHostGet struct {
	*reqZabbix
	Method string        `json:"method"`
	Params reqHostParams `json:"params"`
}

type reqHostDelete struct {
	/*
		Jsonrpc string   `json:"jsonrpc"`
		Method  string   `json:"method"`
		Params  []string `json:"params"`
		Auth    string   `json:"auth"`
		ID      int      `json:"id"`
	*/
	*reqZabbix
	Method string `json:"method"`
	//Params reqHostParams `json:"params"`
	Params []string `json:"params"`
}

type resHostGet struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  []struct {
		Hostid    string `json:"hostid"`
		Host      string `json:"host"`
		Available string `json:"available"`
		Status    string `json:"status"`
		Groups    []struct {
			Groupid string `json:"groupid"`
			Name    string `json:"name"`
		} `json:"groups"`
		Interfaces []struct {
			IP string `json:"ip"`
		} `json:"interfaces"`
		ParentTemplates []struct {
			Name       string `json:"name"`
			Templateid string `json:"templateid"`
		} `json:"parentTemplates"`
	} `json:"result"`
	ID int `json:"id"`
}

type resJSON struct {
	Result struct {
		Hostids []string `json:"hostids"`
	} `json:"result"`
}

var ip string
var tags string

//var csv bool

func init() {
	// Event
	rootCmd.AddCommand(host)
	host.AddCommand(host_get, host_create, host_delete, host_update)

	host_create.Flags().StringVarP(&name, "name", "n", "", "VM Name")
	host_create.MarkFlagRequired("name")
	host_create.Flags().StringVarP(&groupid, "groupid", "g", "", "groupid")
	host_create.MarkFlagRequired("groupid")
	host_create.Flags().StringVarP(&ip, "ip", "", "", "IP")
	host_create.MarkFlagRequired("ip")

	host_get.Flags().StringVarP(&name, "name", "n", "", "VM Name")
	//host_get.Flags().BoolVarP(&csv, "csv", "", false, "csv output")

	// host.delete
	host_delete.Flags().StringVarP(&name, "name", "n", "", "VM Name")
	host_delete.MarkFlagRequired("name")

	// host.update
	host_update.Flags().StringVarP(&name, "name", "n", "", "VM Name")
	host_update.MarkFlagRequired("name")
	host_update.Flags().StringVarP(&group, "group", "g", "", "Host Group")
	host_update.Flags().StringVarP(&templateName, "template", "t", "", "Template name")
	host_update.Flags().StringVarP(&tags, "tags", "", "", "Tag:Value, ... ")
	//host_update.MarkFlagRequired("group")
	//host_update.MarkFlagRequired("template")
}

var host = &cobra.Command{
	Use:   "host",
	Short: "[get,create,delete,update]",
}

var host_create = &cobra.Command{
	Use:   "create",
	Short: "host.create",
	Run: func(cmd *cobra.Command, args []string) {
		groupids := []reqHostGroup{
			{
				Groupid: groupid,
			},
		}
		interfaces := []reqHostInterface{
			{
				Type:  "1",
				Main:  "1",
				UseIP: "1",
				IP:    ip,
				DNS:   "",
				Port:  "10050",
			},
		}

		//var groupid reqHostGroups

		//groupid = append(groupid,reqHostGroups{groupnam
		params := reqHostCreateParams{
			Host:       name,
			Groups:     groupids,
			Interfaces: interfaces,
		}
		req := reqHostCreate{rz, HOST_CREATE_METHOD, params}
		encoded_params, err := json.Marshal(req)
		fmt.Println(string(encoded_params))
		if err != nil {
			log.Fatal(err)
		}
		res, err := sendRequest(encoded_params)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(res))
	},
}

var host_get = &cobra.Command{
	Use:   "get",
	Short: "host.get",
	Run: func(cmd *cobra.Command, args []string) {

		params := reqHostParams{
			Output:                "extend",
			SelectInterfaces:      []string{"ip"},
			SelectGroups:          "extend",
			SelectParentTemplates: []string{"templateid", "name"},
		}
		req := reqHostGet{rz, HOST_GET_METHOD, params}

		if name != "" {
			req.Params.Filter.Host = append(req.Params.Filter.Host, name)
		}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		var data resHostGet
		err = json.Unmarshal([]byte(res), &data)
		if err != nil {
			log.Fatal(err)
		}
		if len(data.Result) == 0 {
			log.Fatal(name + " VM does not exist")
		}
		//fmt.Printf("%5s %20s %15s %30s %s\n", "ID", "Host", "IP", "Group", "Available")
		fmt.Printf("%5s %20s %15s %30s %s\n", "ID", "Host", "IP", "Group", "Status")
		for _, v := range data.Result {
			var group_names_join string
			var group_names []string
			for _, g := range v.Groups {
				group_names = append(group_names, g.Name)
			}
			group_names_join = strings.Join(group_names, ",")

			//s, _ := strconv.Atoi(v.Available)
			//fmt.Printf("%5v %20v %15v %30v %v\n", v.Hostid, v.Host, v.Interfaces[0].IP, group_names_join, available[s])
			host_status := "有効"
			if v.Status == "1" {
				host_status = "無効"
			}

			//if csv == true {
			//	fmt.Println("csv")
			//	os.Exit(1)
			//}

			//fmt.Printf("%5v %20v %15v %30v %v\n", v.Hostid, v.Host, v.Interfaces[0].IP, group_names_join, host_status)

			fmt.Printf("%5v\t%20v\t%15v\t%30v\t%v\n", v.Hostid, v.Host, v.Interfaces[0].IP, group_names_join, host_status)
		}

	},
}
var host_delete = &cobra.Command{
	Use:   "delete",
	Short: "host.delete",
	Run: func(cmd *cobra.Command, args []string) {
		hostid := getHostID(name)[0]
		req := reqHostDelete{rz, HOST_DELETE_METHOD, []string{hostid}}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Println(string(res))
	},
}

var host_update = &cobra.Command{
	Use:   "update",
	Short: "host.update",
	Run: func(cmd *cobra.Command, args []string) {
		/*
			params := reqJSON{
				Jsonrpc: JSONRPC,
				Method:  HOST_UPDATE_METHOD,
				ID:      ID,
				Auth:    Config.APIKEY,
				Params: reqParams{
					Hostid: host_ids[0],
				},
			}
		*/
		//host_ids := getHostID(name)
		reqparams := reqParams{
			Hostid: getHostID(name)[0],
		}
		params := reqJSON{rz, HOST_UPDATE_METHOD, reqparams}

		if group != "" {
			for _, v := range strings.Split(group, ",") {
				groupid := getGroupID(v)
				params.Params.Groups = append(params.Params.Groups, Groupid{ID: groupid})
			}
		}
		if templateName != "" {
			for _, v := range strings.Split(templateName, ",") {
				templateid := getTemplateID(v)
				params.Params.Templates = append(params.Params.Templates, Templateid{ID: templateid})
			}
		}

		params.Params.Tags = reqParamsTags(tags)

		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Printf("%+v\n", string(res))
	},
}

func getHostID(name string) []string {

	var host_names, host_ids []string
	for _, v := range strings.Split(name, ",") {
		host_names = append(host_names, v)
	}
	params := reqHostParams{
		Output:                "extend",
		SelectInterfaces:      []string{"ip"},
		SelectGroups:          "extend",
		SelectParentTemplates: []string{"templateid", "name"},
		Filter: reqHostFilter{
			Host: host_names,
		},
	}
	req := reqHostGet{rz, HOST_GET_METHOD, params}

	encoded_params, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := sendRequest(encoded_params)
	var data resHostGet
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		log.Fatal(err)
	}
	if len(data.Result) == 0 {
		log.Fatal(name + " VM does not exist")
	}
	for _, v := range data.Result {
		host_ids = append(host_ids, v.Hostid)
	}
	return host_ids
	//return data.Result[0].Hostid
}
