package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

type reqItem struct {
	*reqZabbix
	Method string    `json:"method"`
	Params reqParams `json:"params"`
}

type reqItemDelete struct {
	*reqZabbix
	Method string   `json:"method"`
	Params []string `json:"params"`
}

type resItem struct {
	Result []struct {
		Itemid string `json:"itemid"`
		Name   string `json:"name"`
	} `json:"result"`
}

var (
	templateid   string
	itemid       string
	key          string
	hostname     string
	newname      string
	templateName string
)

func init() {
	rootCmd.AddCommand(item)
	item.AddCommand(item_get, item_create, item_delete, item_update)

	item_get.Flags().StringVarP(&hostname, "hostname", "", "", "アイテムが所属するホスト")
	item_get.Flags().StringVarP(&templateName, "templateName", "", "", "アイテム所属するテンプレート")

	item_create.Flags().StringVarP(&name, "name", "n", "", "Item name")
	item_create.MarkFlagRequired("name")
	item_create.Flags().StringVarP(&key, "key", "k", "", "Item Key")
	item_create.MarkFlagRequired("key")

	item_create.Flags().StringVarP(&hostname, "hostname", "", "", "アイテムが所属するホスト")
	item_create.Flags().StringVarP(&templateName, "templateName", "", "", "アイテム所属するテンプレート")

	item_update.Flags().StringVarP(&name, "name", "n", "", "更新するアイテム名")
	item_update.MarkFlagRequired("name")
	item_update.Flags().StringVarP(&newname, "newname", "", "", "新しいアイテム名")
	item_update.Flags().StringVarP(&hostname, "hostname", "", "", "アイテムが所属するホスト名")
	item_update.Flags().StringVarP(&templateName, "templateName", "", "", "アイテムが所属するテンプレート名")
	item_update.Flags().StringVarP(&key, "key", "k", "", "Item Key")
	item_update.Flags().StringVarP(&tags, "tags", "", "", "Tag:Value, ... ")

	item_delete.Flags().StringVarP(&hostname, "hostname", "", "", "アイテムが所属するホスト")
	item_delete.Flags().StringVarP(&templateName, "templateName", "", "", "アイテム所属するテンプレート")
	item_delete.Flags().StringVarP(&name, "name", "n", "", "削除するアイテム名")
	item_delete.MarkFlagRequired("name")
}

var item = &cobra.Command{
	Use:   "item",
	Short: "[ get, create, delete, update]",
}

var item_get = &cobra.Command{
	Use:   "get",
	Short: ITEM_GET_METHOD,
	Run: func(cmd *cobra.Command, args []string) {
		if hostname == "" && templateName == "" {
			cmd.Help()
			return
		}

		params := reqParams{
			Output: "extend",
		}

		if templateName != "" {
			params.Templateids = []string{getTemplateID(templateName)}
		}
		if hostname != "" {
			params.Hostids = []string{getHostID(hostname)[0]}
		}
		req := reqItem{rz, ITEM_GET_METHOD, params}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}

		res, err := sendRequest(encoded_params)
		if err != nil {
			log.Fatal(err)

		}
		var data resItem
		err = json.Unmarshal(res, &data)
		if err != nil {
			log.Fatal(err)
		}
		for _, v := range data.Result {
			fmt.Printf("%s %s\n", v.Itemid, v.Name)
		}

	},
}

// template に作成する場合、templdateid を hostid としてリクエストする
// curl  -H "Content-Type:application/json-rpc"  -v http://localhost/zabbix/api_jsonrpc.php -d '{"auth":"d3d55f889554b65f669ab94200e6c110","jsoner":{},"name":"test","type":2,"key_":"billing.test","value_type":3,"trapper_hosts":"127.0.0.1","hostid":"20327","delay":"30s"}}'
var item_create = &cobra.Command{
	Use:   "create",
	Short: ITEM_CREATE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {

		if templateName == "" && hostname == "" {
			cmd.Help()
			return
		}

		var hostid string
		if hostname != "" {
			hostid = getHostID(hostname)[0]
		}
		if templateName != "" {
			hostid = getTemplateID(templateName)
		}
		params := reqParams{
			Name:         name,
			Type:         ZABBIX_TRAPPER,
			Key:          key,
			ValueType:    ITEM_NUMERIC,
			TrapperHosts: "127.0.0.1",
			Hostid:       hostid,
			Trends:       "1095d",
			History:      "90d",
		}
		req := reqItem{rz, ITEM_CREATE_METHOD, params}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}

		//fmt.Println(string(encoded_params))
		res, err := sendRequest(encoded_params)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(res))

	},
}

var item_delete = &cobra.Command{
	Use:   "delete",
	Short: ITEM_DELETE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {
		if templateName == "" && hostname == "" {
			cmd.Help()
			return
		}

		if hostname != "" {
			itemid = getItemID(name, getHostID(hostname)[0])
		}
		if templateName != "" {
			itemid = getItemID(name, getTemplateID(templateName))
		}

		var params []string
		params = []string{itemid}

		req := reqItemDelete{rz, ITEM_DELETE_METHOD, params}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}
		res, err := sendRequest(encoded_params)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(res))

	},
}

var item_update = &cobra.Command{
	Use:   "update",
	Short: ITEM_UPDATE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {
		if hostname == "" && templateName == "" {
			cmd.Help()
		}
		var hostid string
		if hostname != "" {
			hostid = getHostID(hostname)[0]
		}
		if templateName != "" {
			hostid = getTemplateID(templateName)
		}

		itemid := getItemID(name, hostid)
		params := reqParams{
			Name:   newname,
			Itemid: itemid,
			Tags:   reqParamsTags(tags),
		}

		if key != "" {
			params.Key = key
		}

		req := reqItem{rz, ITEM_UPDATE_METHOD, params}
		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(encoded_params))
		res, err := sendRequest(encoded_params)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(res))

	},
}

func getItemID(name string, hostid string) string {

	params := reqParams{
		Output: "extend",
		Filter: &reqFilter{
			Name:   []string{name},
			Hostid: hostid,
		},
	}
	req := reqItem{rz, ITEM_GET_METHOD, params}

	encoded_params, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := sendRequest(encoded_params)
	var data resItem
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		log.Fatal(err)
	}
	if len(data.Result) == 0 {
		log.Fatal(hostname + " item does not exist")
	}

	return data.Result[0].Itemid

}
