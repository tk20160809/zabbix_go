package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

type reqMaintenanceCreate struct {
	Auth    string                     `json:"auth"`
	Jsonrpc string                     `json:"jsonrpc"`
	Method  string                     `json:"method"`
	ID      int                        `json:"id"`
	Params  reqMaintenanceCreateParams `json:"params"`
}
type reqMaintenanceCreateParams struct {
	Name        string       `json:"name"`
	ActiveSince string       `json:"active_since"`
	ActiveTill  string       `json:"active_till"`
	HostIDs     []string     `json:"hostids"`
	TimePeriods []timePeriod `json:"timeperiods,omitempty"`

	// maintenance.update
	MaintenanceID string `json:"maintenanceid,omitempty"`
}
type resMaintenanceGet struct {
	Result []struct {
		MaintenanceID string `json:"maintenanceid"`
		Name          string `json:"name"`
		ActiveSince   string `json:"active_since"`
		ActiveTill    string `json:"active_till"`
		Hosts         []struct {
			Host string `json:"host"`
		}
		TimePeriods []timePeriod `json:"timeperiods"`
	}
}
type timePeriod struct {
	StartTime string `json:"start_time,omitempty"`
	Period    string `json:"period"`
	StartDate string `json:"start_date"`

	// create
	TimePeriodType string `json:"timeperiod_type,omitempty"`

	DayOfWeek string `json:"dayofweek,omitempty"`
}

type reqMaintenanceDelete struct {
	Auth    string   `json:"auth"`
	Jsonrpc string   `json:"jsonrpc"`
	Method  string   `json:"method"`
	ID      int      `json:"id"`
	Params  []string `json:"params"`
}

var maintenance = &cobra.Command{
	Use:   "maintenance",
	Short: "[create,delete,get,delete,update]",
}

const DATE_LAYOUT = "2006-01-02 15:04"

func init() {
	rootCmd.AddCommand(maintenance)
	maintenance.AddCommand(maintenance_get, maintenance_create, maintenance_delete, maintenance_update)
	maintenance_create.Flags().StringVarP(&mname, "mname", "m", "", "Maintenance Name")
	maintenance_create.MarkFlagRequired("mname")
	maintenance_create.Flags().StringVarP(&name, "name", "n", "", "VM Name (i.e. -n vm1,vm2,vm3)")
	maintenance_create.MarkFlagRequired("name")
	maintenance_create.Flags().StringVarP(&start, "start", "s", "", "StartDate(YYYY-mm-dd-HH-MM)")
	maintenance_create.MarkFlagRequired("start")
	maintenance_create.Flags().StringVarP(&end, "end", "e", "", "EndDate(YYYY-mm-dd-HH-MM)")
	maintenance_create.MarkFlagRequired("end")

	maintenance_delete.Flags().StringVarP(&id, "id", "i", "", "MaintenanceID")
	maintenance_delete.MarkFlagRequired("id")

	maintenance_update.Flags().StringVarP(&id, "id", "i", "", "MaintenanceID")
	maintenance_update.MarkFlagRequired("id")
	maintenance_update.Flags().StringVarP(&name, "name", "n", "", "VM Name (i.e. -n vm1,vm2,vm3)")
	maintenance_update.MarkFlagRequired("name")

}

/*
   月  1   0000001
   火  2   0000010
   水  4   0000100
   木  8   0001000
   金 16   0010000
   土 32   0100000
   日 64   1000000
*/
func dayOfWeek(daystr string) string {
	w := []string{}
	daybit, _ := strconv.Atoi(daystr)
	for _, v := range [7]string{"mon", "tue", "wed", "thu", "fri", "sat", "sun"} {
		if daybit&1 == 1 {
			w = append(w, v)
		}
		daybit = daybit >> 1
	}
	return strings.Join(w, ",")
}

func periodCalc(time_periods []timePeriod) []timePeriod {

	period_types := [5]string{"OneTime", "", "Daily", "Weekly", "Monthly"}

	var periods []timePeriod
	for _, v := range time_periods {
		period_type_num, _ := strconv.Atoi(v.TimePeriodType)
		period_type := period_types[period_type_num]

		period_sec, _ := strconv.Atoi(v.Period)
		period_min := period_sec / 60
		period_hour := period_min / 60
		period_sur := period_min % 60
		period := fmt.Sprintf("%dh%dm", period_hour, period_sur)

		st_sec, _ := strconv.Atoi(v.StartTime)
		st_min := st_sec / 60
		st_hour := st_min / 60
		st_sur := st_min % 60
		st := fmt.Sprintf("%02d:%02d", st_hour, st_sur)

		var sd string
		if period_type_num != 0 {
			sd = ""
		} else {
			start_date_int64, _ := strconv.ParseInt(v.StartDate, 10, 64)
			sd = time.Unix(start_date_int64, 0).Format(DATE_LAYOUT)
		}

		dayofweek := dayOfWeek(v.DayOfWeek)

		periods = append(periods, timePeriod{
			StartTime:      st,
			Period:         period,
			StartDate:      sd,
			TimePeriodType: period_type,
			DayOfWeek:      dayofweek,
		},
		)
	}

	return periods

}

var maintenance_get = &cobra.Command{
	Use:   "get",
	Short: "maintenance.get",
	Run: func(cmd *cobra.Command, args []string) {
		/*
			params := reqJSON{
				Jsonrpc: JSONRPC,
				Method:  MAINTENANCE_GET_METHOD,
				ID:      ID,
				Auth:    Config.APIKEY,
				Params: reqParams{
					Output:            "extend",
					SelectTimePeriods: "extend",
					SelectHosts:       "extend",
					SortOrder:         "DESC",
					SortField:         "maintenanceid",
				},
			}
		*/

		params := reqParams{
			Output:            "extend",
			SelectTimePeriods: "extend",
			SelectHosts:       "extend",
			SortOrder:         "DESC",
			SortField:         "maintenanceid",
		}
		req := reqJSON{rz, MAINTENANCE_GET_METHOD, params}

		encoded_params, err := json.Marshal(req)
		if err != nil {
			log.Fatal(err)
		}

		res, _ := sendRequest(encoded_params)
		//fmt.Println(string(res))
		var data resMaintenanceGet
		err = json.Unmarshal([]byte(res), &data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%5s %10s %20s %20s %20s %10s %10s %10s %s %s\n", "ID", "PeriodType", "ActiveSince", "ActiveTill", "StartDate", "StartTime", "Period", "DayOfWeek", "Name", "Host")
		for _, v := range data.Result {
			host_names_join := ""
			host_names := []string{}
			for _, h := range v.Hosts {
				host_names = append(host_names, h.Host)
			}

			start_int64, _ := strconv.ParseInt(v.ActiveSince, 10, 64)
			start := time.Unix(start_int64, 0).Format(DATE_LAYOUT)

			end_int64, _ := strconv.ParseInt(v.ActiveTill, 10, 64)
			end := time.Unix(end_int64, 0).Format(DATE_LAYOUT)

			host_names_join = strings.Join(host_names, ",")

			p := periodCalc(v.TimePeriods)
			fmt.Printf("%5v %10v %20v %20v %20v %10v %10v %10v %v %v\n",
				v.MaintenanceID, p[0].TimePeriodType, start, end, p[0].StartDate, p[0].StartTime, p[0].Period, p[0].DayOfWeek, v.Name, host_names_join)

			if len(v.TimePeriods) > 1 {
				//fmt.Println(v)
				for _, v := range p[1:] {
					fmt.Printf("%5v %10v %20v %20v %20v %10v %10v %10v %v %v\n",
						"", v.TimePeriodType, "", "", "", v.StartTime, v.Period, v.DayOfWeek, "", "")
				}
			}
		}
	},
}

func datetounix(d string) int64 {

	var t []int
	date_arr := strings.Split(d, "-")
	if len(date_arr) != 5 {
		log.Fatal(d + " invalid format")
	}
	for _, v := range date_arr {
		i, _ := strconv.Atoi(v)
		t = append(t, i)
	}
	u := time.Date(t[0], time.Month(t[1]), t[2], t[3], t[4], 0, 0, time.Local).Unix()
	return u
}

var maintenance_create = &cobra.Command{
	Use:   "create",
	Short: "maintenance.create",
	Run: func(cmd *cobra.Command, args []string) {

		s := datetounix(start)
		e := datetounix(end)
		if e < s {
			log.Fatal("-e EndDate must be after -s StartDate")
		}

		// int64 to string
		period := strconv.FormatInt(e-s, 10)
		start_time := strconv.FormatInt(s, 10)
		end_time := strconv.FormatInt(e, 10)

		host_ids := getHostID(name)
		params := reqMaintenanceCreate{
			Jsonrpc: JSONRPC,
			Method:  MAINTENANCE_CREATE_METHOD,
			ID:      ID,
			Auth:    Config.APIKEY,
			Params: reqMaintenanceCreateParams{
				Name:        mname,
				ActiveSince: start_time,
				ActiveTill:  end_time,
				HostIDs:     host_ids,
				TimePeriods: []timePeriod{
					{
						StartDate: start_time,
						Period:    period,
					},
				},
			},
		}
		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Printf("%+v\n", string(res))
	},
}

var maintenance_delete = &cobra.Command{
	Use:   "delete",
	Short: "maintenance.delete",
	Run: func(cmd *cobra.Command, args []string) {
		params := reqMaintenanceDelete{
			Jsonrpc: JSONRPC,
			Method:  MAINTENANCE_DELETE_METHOD,
			ID:      ID,
			Auth:    Config.APIKEY,
			Params:  []string{id},
		}
		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Printf("%+v\n", string(res))
	},
}

// update hosts only
var maintenance_update = &cobra.Command{
	Use:   "update",
	Short: "maintenance.update",
	Run: func(cmd *cobra.Command, args []string) {
		host_ids := getHostID(name)
		m := getMaintenanceInfo(id)
		params := reqMaintenanceCreate{
			Jsonrpc: JSONRPC,
			Method:  MAINTENANCE_UPDATE_METHOD,
			ID:      ID,
			Auth:    Config.APIKEY,
			Params: reqMaintenanceCreateParams{
				Name:          m["name"],
				ActiveSince:   m["active_since"],
				ActiveTill:    m["active_till"],
				MaintenanceID: id,
				HostIDs:       host_ids,
				TimePeriods: []timePeriod{
					{
						StartDate: m["start_date"],
						Period:    m["period"],
					},
				},
			},
		}
		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Printf("%+v\n", string(res))
	},
}

func getMaintenanceInfo(id string) map[string]string {

	var result map[string]string
	/*
		params := reqJSON{
			Jsonrpc: JSONRPC,
			Method:  MAINTENANCE_GET_METHOD,
			ID:      ID,
			Auth:    Config.APIKEY,
			Params: reqParams{
				Output:            "extend",
				SelectTimePeriods: "extend",
				Filter: &reqFilter{
					MaintenanceID: []string{id},
				},
			},
		}
		encoded_params, err := json.Marshal(params)
	*/

	params := reqParams{
		Output:            "extend",
		SelectTimePeriods: "extend",
		Filter: &reqFilter{
			MaintenanceID: []string{id},
		},
	}
	req := reqJSON{rz, MAINTENANCE_GET_METHOD, params}

	encoded_params, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := sendRequest(encoded_params)
	var data resMaintenanceGet
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		log.Fatal(err)
	}
	result = map[string]string{
		"name":         data.Result[0].Name,
		"active_since": data.Result[0].ActiveSince,
		"active_till":  data.Result[0].ActiveTill,
		"start_date":   data.Result[0].TimePeriods[0].StartDate,
		"start_time":   data.Result[0].TimePeriods[0].StartTime,
		"period":       data.Result[0].TimePeriods[0].Period,
	}

	return result

}
