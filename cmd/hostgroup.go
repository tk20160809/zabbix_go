package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

/*
type reqGetGroupID struct {
	*reqZabbix
	METHOD string `json:"method"`
	//Params reqGroupGetParams `json:"params"`
	Params reqParams `json:"params"`
}
*/

type reqGroupGetParams struct {
	Filter reqGroupGetParamsFilter `json:"filter"`
}
type reqGroupGetParamsFilter struct {
	Name []string `json:"name"`
}

type reqGroupGet struct {
	*reqZabbix
	Method string         `json:"method"`
	Params reqGroupParams `json:"params"`
}
type reqGroupParams struct {
	Output string `json:"output"`
}

type reqGroupUpdate struct {
	*reqZabbix
	Method string               `json:"method"`
	Params reqGroupUpdateParams `json:"params"`
}
type reqGroupUpdateParams struct {
	Groupid string `json:"groupid"`
	Name    string `json:"name"`
}

type resGroupGet struct {
	Result []struct {
		Groupid string `json:"groupid"`
		Name    string `json:"name"`
	} `json:"result"`
}

// グループ名を受け取りグループIDを返す
func getGroupID(name string) string {
	params := reqParams{
		Filter: &reqFilter{
			Name: []string{name},
		},
	}
	//req := reqGetGroupID{rz, HOSTGROUP_GET_METHOD, params}
	req := reqJSON{rz, HOSTGROUP_GET_METHOD, params}
	encoded_params, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := sendRequest(encoded_params)
	var data *resGroupGet
	err = json.Unmarshal([]byte(res), &data)
	if err != nil {
		log.Fatal(err)
	}
	if len(data.Result) == 0 {
		log.Fatal(name + " group does not exist")
	}
	return data.Result[0].Groupid

}

var hostgroup = &cobra.Command{
	Use:   "hostgroup",
	Short: "[ get,update ]",
}

var hostgroup_get = &cobra.Command{
	Use:   "get",
	Short: HOSTGROUP_GET_METHOD,
	Run: func(cmd *cobra.Command, args []string) {

		rgp := reqGroupParams{"extend"}
		params := reqGroupGet{rz, HOSTGROUP_GET_METHOD, rgp}

		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		//res := sendRequest(string(encoded_params))
		res, _ := sendRequest(encoded_params)
		var data resGroupGet
		err = json.Unmarshal([]byte(res), &data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%5s %30s\n", "ID", "Name")
		for _, v := range data.Result {
			fmt.Printf("%5s %30s\n", v.Groupid, v.Name)
		}

	},
}

var hostgroup_update = &cobra.Command{
	Use:   "update",
	Short: HOSTGROUP_UPDATE_METHOD,
	Run: func(cmd *cobra.Command, args []string) {
		rgu := reqGroupUpdateParams{id, name}
		//rg := &reqGroup{Config.APIKEY, JSONRPC, HOSTGROUP_UPDATE_METHOD, ID}
		//params := reqGroupUpdate{rg, rgu}
		params := reqGroupUpdate{rz, HOSTGROUP_UPDATE_METHOD, rgu}
		encoded_params, err := json.Marshal(params)
		if err != nil {
			log.Fatal(err)
		}
		res, _ := sendRequest(encoded_params)
		fmt.Println(string(res))

	},
}
